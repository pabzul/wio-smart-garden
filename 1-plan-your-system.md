# 1 - Plan your system

<figure><img src=".gitbook/assets/11 (2).png" alt=""><figcaption></figcaption></figure>

## Planning steps:

To plan our smart garden system, we must consider some of the following things:&#x20;

1. The type of measurements we want to obtain to know what sensors we want to use.&#x20;
2. The actions we want to perform with our system such as activating a water pump, turning on a light, a heating or cooling system, an alarm sound, etc.&#x20;
3. What information do we want to transmit and how? since it could be on an integrated screen, in the serial monitor, on an external screen, in an application, a database or an internet dashboard.
4. The number of plants we will control to know the number of necessary components.
5. Where our system will be located to decide if we need to locate it in a specific compartment or what will be its source of energy.



<figure><img src=".gitbook/assets/7.jpg" alt=""><figcaption></figcaption></figure>

## Our System:

In this specific case, we are going to build a system that gets the environmental temperature, relative humidity, light intensity and soil moisture.&#x20;

We won't actuate with any external component at the moment, but we will activate the internal buzzer if the soil moisture of the plant is too low, accompanied by a visual notification on the screen.

We will display on the WIO terminal screen the values from our four sensors: Temperature, humidity, soil moisture and light. And also program the on-screen notification for low soil moisture. In the future, we could connect this device to the internet and publish the data on an App or digital dashboard.

We will only control one plant, in interior conditions and powered by the USB-C cable at the moment.

