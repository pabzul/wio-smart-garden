# WIO Internal Light sensor

This code will let you read the WIO Terminal Integrated Light Sensor and show the results in the serial port of your computer.

<pre class="language-arduino"><code class="lang-arduino"><strong>void setup() {
</strong>  // put your setup code here, to run once:
  
  // declare PinMode
  pinMode(WIO_LIGHT, INPUT);
  
  //Start serial communication
  Serial.begin(9600);
  
}

void loop() {
  // put your main code here, to run repeatedly:

  // declare a variable to store the read values
  int light = analogRead(WIO_LIGHT);
  
  // print in the serial monitor the sensor values 
  Serial.print("Light Value: ");
  Serial.println(light);

  // wait 1 second to read again
  delay(1000);

}
</code></pre>

