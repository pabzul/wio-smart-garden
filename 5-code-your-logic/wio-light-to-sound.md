# WIO Light to sound

We are going to use the light sensor to activate the buzzer depending on the amount of light it gets.

```arduino
void setup() {
  // put your setup code here, to run once:
  
  // declare PinMode
  pinMode(WIO_LIGHT, INPUT);
  pinMode(WIO_BUZZER, OUTPUT);
  
  //Start serial communication
  Serial.begin(9600);
  
}

void loop() {
  // put your main code here, to run repeatedly:

  // declare a variable to store the read values
  int light = analogRead(WIO_LIGHT);
  
  // print in the serial monitor the sensor values 
  Serial.print("Light Value: ");
  Serial.println(light);
  
  if (light > 200) {
  analogWrite(WIO_BUZZER, 100);
  Serial.println("SUNNY");
  }

  else
  {
  analogWrite(WIO_BUZZER, 0);
  Serial.println("DARK");  
  }
  // wait 1 second to read again
  delay(500);

}
```

