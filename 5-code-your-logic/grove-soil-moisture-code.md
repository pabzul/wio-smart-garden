# Grove Soil Moisture code

This code will let you read the Grove Soil Moisture Sensor with the WIO Terminal and show the results in the serial port of your computer.

### Reading the sensor:

```arduino
// declare to which Pin the sensor is connected
int sensorPin = A0;

// declare the initial value of the sensor
int sensorValue = 0;
 
void setup() {
    // open the serial port (the communication with the computer to show values)
    Serial.begin(9600);
}

void loop() {
    // read the value from the sensor:
    sensorValue = analogRead(sensorPin);

    // print the value in the serial monitor
    Serial.print("Moisture = " );
    Serial.println(sensorValue);

    // wait one second to start again
    delay(1000);
}
```

{% file src="../.gitbook/assets/Soil Moisture Read.ino" %}
Code file
{% endfile %}



## Calibrating the sensor:

To properly calibrate the sensor, we would need to check the real values it gives us under different conditions. For that, we can use some cups to fill them with dry soil, moisture soil, and in when it is full of water.

<figure><img src="../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

1. Insert the sensor inside each cup and check the readings on the serial monitor for a few seconds.
2. Write down an average number that represents those readings under each condition.
3. Modify the code below, according to those conditions.

In the next code, we should change the first two values in the mapping function for the values we got from our calibration exercise:

```
    // map the values (TOTALLY WET_reading, TOTALLY DRY_reading, 100%, 0%)
    sensorValue = map(sensorValue, 1023, 0, 100, 0);
```

So you should change the 1030 value for the value of your sensor soaked in water, and the 0 for the value when it is dry... this dry value usually is between 0 and 3, so 0 might work well.

###

### Mapping the readings to a percentage:

This instruction turns the values of the code mapping them on a range from 0% to 100%

```arduino
// declare to which Pin the sensor is connected
int sensorPin = A0;

// declare the initial value of the sensor
int sensorValue = 0;
 
void setup() {
    // open the serial port (the communication with the computer to show values)
    Serial.begin(9600);
}

void loop() {
    // read the value from the sensor:
    sensorValue = analogRead(sensorPin);
    
    // map the values (HIGHEST_reading, LOWEST_reading, 100%, 0%)
    sensorValue = map(sensorValue, 1023, 0, 100, 0);

    // print the value in the serial monitor
    Serial.print("Moisture = " );
    Serial.print(sensorValue);
    Serial.println(" %" );

    // wait one second to start again
    delay(1000);
}
```



### Assigning Functions to Values:

This code will assign some functions to the values, so that it acts differently depending if the soil is dry, in a good wet state or too wet.

```arduino
// declare to which Pin the sensor is connected
int sensorPin = A0;

// declare the initial value of the sensor
int sensorValue = 0;

void setup() {
    // open the serial port (the communication with the computer to show values)
    Serial.begin(9600);
}

void loop() {
    // read the value from the sensor:
    sensorValue = analogRead(sensorPin);
    // map the values (HIGHEST_reading, LOWEST_reading, 100%, 0%)
    sensorValue = map(sensorValue, 1023, 0, 100, 0);

/* a conditional that tells us that the plant is DRY
if the value is LESS or equal to 300 */
if (sensorValue <= 30) {
    Serial.println("DRY SOIL...PLANT NEEDS WHATER" );
    Serial.print("Moisture = " );
    Serial.print(sensorValue);
    Serial.println(" %" );
}

/* a conditional that tells us that the plant is TOO WET 
if the value is MORE or equal to 700 */
if (sensorValue >= 70) {
    Serial.println("SUPER WET...TOO MUCH WATER" );
    Serial.print("Moisture = " );
    Serial.print(sensorValue);
    Serial.println(" %" );
}

/* a conditional that tells us that the plant is HAPPY 
if the value is LESS than 700 and MORE than 300 */
if (sensorValue < 70 && sensorValue > 30) {
    Serial.println("PERFECT MOISTURE...PLANT IS HAPPY" );
    Serial.print("Moisture = " );
    Serial.print(sensorValue);
    Serial.println(" %" );
}

    // a space between the writing of the readings
    Serial.println();

    // wait one second to start again
    delay(1000);
}
```

{% file src="../.gitbook/assets/Soil Moisture Mapped.ino" %}
Code File
{% endfile %}

