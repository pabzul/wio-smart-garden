# WIO internal buzzer

This code will let you actuate the WIO Terminal Integrated Buzzer

<pre class="language-arduino"><code class="lang-arduino">void setup() {
    pinMode(WIO_BUZZER, OUTPUT);
}
 
void loop() {
    analogWrite(WIO_BUZZER, 128);
<strong>    delay(1000);
</strong>    analogWrite(WIO_BUZZER, 0);
    delay(1000);
}
</code></pre>

