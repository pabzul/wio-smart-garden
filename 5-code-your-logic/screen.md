# Screen



### Display a text on a colour background:

This code will load the needed library to use the screen and will set the background colour and rotation, font size and colour, and display text on the desired location of the screen.

```arduino
#include "TFT_eSPI.h"

TFT_eSPI tft;

void setup() {
  
  tft.begin();
  tft.setRotation(3);

  tft.fillScreen(TFT_GREEN);
  tft.setTextSize(4);
  tft.setTextColor(TFT_WHITE);
  tft.drawString("POWAR GARDEN", 20, 100);
}

void loop() {
}
```



## Light Sensor + LCD:

This code will read the light sensor value, and display its information on the LCD screen. This will show you how the basic integration of codes we have tried works on the LCD.

```arduino
#include "TFT_eSPI.h"

TFT_eSPI tft;

void setup() {
tft.begin();
tft.setRotation(3);
tft.fillScreen(TFT_GREEN);
}

void loop() {
int lightVal = analogRead(WIO_LIGHT);
tft.fillScreen(TFT_GREEN);
tft.setCursor(tft.width() / 2, tft.height() / 2);
tft.setTextColor(TFT_WHITE);
tft.setTextSize(4);
tft.println(lightVal);

delay(50);
}
```

