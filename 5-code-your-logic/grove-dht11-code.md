# Grove DHT11 code

This code will let you read the Grove DHT11 with the WIO Terminal and show the results in the serial port of your computer.

```arduino
#include "DHT.h"          //DHT library

#define DHTPIN D0         // define the digital pin 0

#define DHTTYPE DHT11     //Define the DHT sensor type

DHT dht(DHTPIN, DHTTYPE); // initialize DHT sensor


void setup() {
  // put your setup code here, to run once:
  
  //Start serial communication
  Serial.begin(9600);
  
  //Start DHT sensor
  dht.begin();

}

void loop() {
  // put your main code here, to run repeatedly:

//create an integer T to store the Temperature value
  int t = dht.readTemperature();
  
//create an integer H to store the Humidity value
  int h = dht.readHumidity();

// Print Temperature and Humidity Values
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println("ºC");
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.println("%");

  delay(2000);
}
```



