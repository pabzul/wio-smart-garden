# FULL CODE

## Code using DHT11:

This code will initialize by loading the libraries, then set the PinModes for the internal LIGH sensor and BUZZER as Inputs and Outputs, then start the DHT sensor, start the screen and set the initial rotation and size, and draw an initial welcome message.

Then, in the VoidLoop, it will fill the screen with White, draw a Green rectangle on top, draw some vertical and horizontal lines, and then read and map each of the sensors and display their values.

At the end of the code, we added a conditional so that if the Soil sensor reading is too low (DRY) it will display a message on the screen and start beeping.

You should also consider modifying this code with the calibration values you've got from the previous calibration exercise.

```arduino
#include "TFT_eSPI.h"
#include "DHT.h"

// Set the I2C pin into a Digital/Analogue Pin
#define DHTPIN PIN_WIRE_SCL

// Define the kind of DHT sensor you use
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);
TFT_eSPI tft;
TFT_eSprite spr = TFT_eSprite(&tft);

int moisturePin = A0;
int moistureVal = 0;



void setup() {
  // declare the pinModes of the internal LIGH sensor and BUZZER 
  pinMode(WIO_LIGHT, INPUT);
  pinMode(WIO_BUZZER, OUTPUT);

  // Begin DHT sensor and Screen
  dht.begin();
  tft.begin();
  
  // Set initial screen parameters
  tft.setRotation(3);
  spr.createSprite(TFT_HEIGHT, TFT_WIDTH);
  
  // Draw the initial Screen
  tft.fillScreen(TFT_GREEN);
  tft.setTextSize(4);
  tft.setTextColor(TFT_WHITE);
  tft.drawString("POWAR GARDEN", 20, 100);
  tft.setTextSize(2);
  tft.drawString("With Seeed Studio", 60, 180);
  
  // Beep once when it starts
  analogWrite(WIO_BUZZER, 150);
  delay(100);
  analogWrite(WIO_BUZZER, 0);
  delay(2000);
  
  // Draw the Maker Faire Screen
  tft.fillScreen(TFT_BLUE);
  tft.setTextSize(3);
  tft.setTextColor(TFT_RED);
  tft.fillRect(20, 20, 280, 200, TFT_WHITE);
  tft.drawString("Maker Faire", 65, 90);
  tft.setTextColor(TFT_BLUE);
  tft.drawString("LISBON", 110, 130);

  delay(1500);
  
}


void loop() {

  // Declare the initial Variables for the code and MAP the Values
  int t = dht.readTemperature();
  int h = dht.readHumidity();

  // This code will MAP the SOIL MOISTURE values
  mnoistureVal = analogRead(mnoisturePin);
  mnoistureVal = map(mnoistureVal, 1023, 0, 100, 0);

  // This code will MAP the LIGH SENSOR values
  int light = analogRead(WIO_LIGHT);
  light = map(light, 0, 1023, 0, 100);


  // This will set the screen to White and draw a DarkGreen rectangle on top
  spr.fillSprite(TFT_WHITE);
  spr.fillRect(0, 0, 320, 50, TFT_DARKGREEN);
  spr.setTextSize(3);
  spr.setTextColor(TFT_WHITE);
  spr.drawString("POWAR GARDEN", 55, 15);


  // This will draw vertical and horizontal lines to divide the screen
  spr.drawFastVLine(150, 50, 190, TFT_DARKGREEN);
  spr.drawFastHLine(0, 140, 320, TFT_DARKGREEN);


  // Draw TEMPERATURE Value
  spr.setTextColor(TFT_BLACK);
  spr.setTextSize(2);
  spr.drawString("Temperature", 10, 65);
  spr.setTextSize(3);
  spr.drawNumber(t, 50, 95);
  spr.drawString("C", 90, 95);


  // Draw HIMIDITY Value
  spr.setTextSize(2);
  spr.drawString("Humidity", 25, 160);
  spr.setTextSize(3);
  spr.drawNumber(h, 30, 190);
  spr.drawString("%RH", 70, 190);


  // Draw SOIL MOISTURE Value
  spr.setTextSize(2);
  spr.drawString("Soil Moisture", 160, 65);
  spr.setTextSize(3);
  spr.drawNumber(mnoistureVal, 200, 95);
  spr.drawString("%", 240, 95);


  // Draw LIGHT Value
  spr.setTextSize(2);
  spr.drawString("Light", 200, 160);
  spr.setTextSize(3);
  spr.drawNumber(light, 205, 190);
  spr.drawString("%", 245, 190);


  // This is the conditionals for the soil moisture alarm
  if (sensorVal < 40) {
    spr.fillSprite(TFT_DARKGREEN);
    spr.setTextColor(TFT_WHITE);
    spr.drawString("SAD PLANT", 80, 100);
    analogWrite(WIO_BUZZER, 0);
    spr.pushSprite(0, 0);
    delay(1000);

    spr.fillSprite(TFT_RED);
    spr.setTextColor(TFT_YELLOW);
    spr.drawString("TIME TO WATER!", 40, 100);
    analogWrite(WIO_BUZZER, 150);
    spr.pushSprite(0, 0);
    delay(1000);
    analogWrite(WIO_BUZZER, 0);
  }

  spr.pushSprite(0, 0);
  delay(50);
}
```



## Code using an SHT40 instead of DHT11:



```cpp
#include "TFT_eSPI.h"
#include <Arduino.h>
#include <SensirionI2CSht4x.h>
#include <Wire.h>

SensirionI2CSht4x sht4x;
TFT_eSPI tft;
TFT_eSprite spr = TFT_eSprite(&tft);

int moisturePin = A0;
int moistureVal = 0;



void setup() {
  // put your setup code here, to run once:

  Wire.begin();

  uint16_t error;
  char errorMessage[256];

  sht4x.begin(Wire);

  uint32_t serialNumber;
  error = sht4x.serialNumber(serialNumber);
  if (error) {
    Serial.print("Error trying to execute serialNumber(): ");
    errorToString(error, errorMessage, 256);
    Serial.println(errorMessage);
  } else {
    Serial.print("Serial Number: ");
    Serial.println(serialNumber);
  }

  // Set initial screen parameters
  tft.begin();
  tft.setRotation(3);
  spr.createSprite(TFT_HEIGHT, TFT_WIDTH);
  
  // Draw the initial Screen
  tft.fillScreen(TFT_GREEN);
  tft.setTextSize(4);
  tft.setTextColor(TFT_WHITE);
  tft.drawString("POWAR GARDEN", 20, 100);
  tft.setTextSize(2);
  tft.drawString("With Seeed Studio", 60, 180);
  
  // Beep once when it starts
  analogWrite(WIO_BUZZER, 150);
  delay(100);
  analogWrite(WIO_BUZZER, 0);
  delay(2000);
  
  // Draw the Maker Faire Screen
  tft.fillScreen(TFT_BLUE);
  tft.setTextSize(3);
  tft.setTextColor(TFT_RED);
  tft.fillRect(20, 20, 280, 200, TFT_WHITE);
  tft.drawString("Maker Faire", 65, 90);
  tft.setTextColor(TFT_BLUE);
  tft.drawString("LISBON", 110, 130);

  delay(1500);
}

void loop() {
  // put your main code here, to run repeatedly:

  uint16_t error;
  char errorMessage[256];

  delay(1000);

  float h;
  float t;
  error = sht4x.measureHighPrecision(t, h);
  if (error) {
    Serial.print("Error trying to execute measureHighPrecision(): ");
    errorToString(error, errorMessage, 256);
    Serial.println(errorMessage);
  } else {
    Serial.print("Temperature:");
    Serial.print(t);
    Serial.print("\t");
    Serial.print("Humidity:");
    Serial.println(h);
  }

  // This code will MAP the SOIL MOISTURE values
  mnoistureVal = analogRead(mnoisturePin);
  mnoistureVal = map(mnoistureVal, 1023, 0, 100, 0);

  // This code will MAP the LIGH SENSOR values
  int light = analogRead(WIO_LIGHT);
  light = map(light, 0, 1023, 0, 100);


  // This will set the screen to White and draw a DarkGreen rectangle on top
  spr.fillSprite(TFT_WHITE);
  spr.fillRect(0, 0, 320, 50, TFT_DARKGREEN);
  spr.setTextSize(3);
  spr.setTextColor(TFT_WHITE);
  spr.drawString("POWAR GARDEN", 55, 15);


  // This will draw vertical and horizontal lines to divide the screen
  spr.drawFastVLine(150, 50, 190, TFT_DARKGREEN);
  spr.drawFastHLine(0, 140, 320, TFT_DARKGREEN);


  // Draw TEMPERATURE Value
  spr.setTextColor(TFT_BLACK);
  spr.setTextSize(2);
  spr.drawString("Temperature", 10, 65);
  spr.setTextSize(3);
  spr.drawNumber(t, 50, 95);
  spr.drawString("C", 90, 95);


  // Draw HIMIDITY Value
  spr.setTextSize(2);
  spr.drawString("Humidity", 25, 160);
  spr.setTextSize(3);
  spr.drawNumber(h, 30, 190);
  spr.drawString("%RH", 70, 190);


  // Draw SOIL MOISTURE Value
  spr.setTextSize(2);
  spr.drawString("Soil Moisture", 160, 65);
  spr.setTextSize(3);
  spr.drawNumber(mnoistureVal, 200, 95);
  spr.drawString("%", 240, 95);


  // Draw LIGHT Value
  spr.setTextSize(2);
  spr.drawString("Light", 200, 160);
  spr.setTextSize(3);
  spr.drawNumber(light, 205, 190);
  spr.drawString("%", 245, 190);


  // This is the conditionals for the soil moisture alarm
  if (sensorVal < 40) {
    spr.fillSprite(TFT_DARKGREEN);
    spr.setTextColor(TFT_WHITE);
    spr.drawString("SAD PLANT", 80, 100);
    analogWrite(WIO_BUZZER, 0);
    spr.pushSprite(0, 0);
    delay(1000);

    spr.fillSprite(TFT_RED);
    spr.setTextColor(TFT_YELLOW);
    spr.drawString("TIME TO WATER!", 40, 100);
    analogWrite(WIO_BUZZER, 150);
    spr.pushSprite(0, 0);
    delay(1000);
    analogWrite(WIO_BUZZER, 0);
  }

  spr.pushSprite(0, 0);
  delay(50);
}
```









