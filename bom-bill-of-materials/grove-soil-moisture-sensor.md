# Grove - Soil Moisture Sensor

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

## Description:

This Moisture Sensor can be used for detecting the moisture of soil or judging if there is water around the sensor, letting the plant in your garden able to reach out for human help when they are thirsty. This sensor is very easy to use, you can just simply insert it into the soil and read the data. With this sensor, you can make a small project that can let the plant send a message to you like " I am thirsty now, please feed me some water."

<figure><img src="../.gitbook/assets/image (27).png" alt=""><figcaption></figcaption></figure>

### Features

* **Cost-effective:** Great for students, makers and IoT developers as DIY and prototype design.
* **Plug-and-play:** Grove-compatible interface(data transmission) and two large pads function as probes(data collecting).
* **Starter-friendly:** Detail start-up tutorials, abundant resources, and projects provided in Wiki and Community.
* **Effective measurement:** Based on soil resistivity measurement, high moisture resulting lower resistance and vice versa.



### Technical details

| Dimensions                        | 60mm x20mm x6.35mm |
| --------------------------------- | ------------------ |
| Weight                            | G.W 10g            |
| Battery                           | Exclude            |
| Operating voltage                 | 3.3\~5V            |
| Operating current                 | 35mA               |
| Sensor Output Value in dry soil   | 0\~ 300            |
| Sensor Output Value in humid soil | 300\~700           |
| Sensor Output Value in water      | 700 \~ 950         |
| PCB size                          | 2.0cm X 6.0cm      |

{% embed url="https://wiki.seeedstudio.com/Grove-Moisture_Sensor/" %}

{% embed url="https://www.seeedstudio.com/Grove-Moisture-Sensor.html" %}
