---
description: Temperature and Relative Humidity sensor
---

# Grove - DHT11

<figure><img src="../.gitbook/assets/image (18).png" alt=""><figcaption></figcaption></figure>

### Description

Grove - Temperature & Humidity Sensor using the upgrade version of DHT11, compare to the old version of DHT11, the new version of DHT11 is using a type of **Capacitive sensor**, and The measurement range of temperature and humidity is **wider** than that of the old version, The temperature resolution is higher. DHT11 is the most common temperature and humidity module for Arduino and Raspberry Pi. It is widely favoured by hardware enthusiasts for its many advantages such as **low power consumption** and excellent **long-term stability**. The single-bus digital signal is output through the **built-in ADC**, which saves the I/O resources of the control board.



<figure><img src="../.gitbook/assets/pingout.jpg" alt=""><figcaption></figcaption></figure>

### Features

* **High measurement accuracy and wide range**: Humidity range of **5** to **95%** RH with a **±5%** and also a temperature range of **-20** to **60℃** with a **±2%**.
* **Save the I/O resources of the control board:** single-bus digital signal output through the built-in ADC
* **High Stability and Low Cost**
* **Long transmission distance and excellent long-term stability**



### Technical details

| Dimensions                  | 40mm x20mm x8mm |
| --------------------------- | --------------- |
| Weight                      | G.W 10g         |
| Battery                     | Exclude         |
| Input Voltage               | 3.3V & 5V       |
| Measuring Current           | 1.3 - 2.1 mA    |
| Measuring Humidity Range    | 5% - 95% RH     |
| Measuring Temperature Range | -20 - 60 ℃      |



{% file src="../.gitbook/assets/Grove_Temperature_And_Humidity_Sensor-master (1).zip" %}

### [Seeed DHT library](https://github.com/Seeed-Studio/Grove\_Temperature\_And\_Humidity\_Sensor)

{% embed url="https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor" %}

{% embed url="https://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/" %}
Wiki - Temperature\&Humidity Sensor (DHT11)
{% endembed %}

{% embed url="https://www.seeedstudio.com/Grove-Temperature-Humidity-Sensor-DHT11.html" %}
Shoping - Temperature\&Humidity Sensor (DHT11)
{% endembed %}
