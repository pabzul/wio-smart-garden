# WIO Terminal

<figure><img src="../.gitbook/assets/image (11).png" alt=""><figcaption></figcaption></figure>

The **Wio Terminal** is a SAMD51-based microcontroller with Wireless Connectivity powered by **Realtek RTL8720DN** that’s compatible with Arduino and MicroPython. Currently, wireless connectivity is only supported by Arduino. It runs at **120MHz** (Boost up to 200MHz), **4MB** External Flash and **192KB** RAM. It supports both Bluetooth and Wi-Fi providing the backbone for IoT projects. The Wio Terminal itself is equipped with **a 2.4” LCD Screen, onboard IMU(LIS3DHTR), Microphone, Buzzer, microSD card slot, Light sensor, and Infrared Emitter(IR 940nm).** On top of that, it also has two multifunctional Grove ports for [Grove Ecosystem](https://www.seeedstudio.com/grove.html) and 40 Raspberry pi compatible pin GPIO for more add-ons.

{% embed url="https://www.youtube.com/watch?v=HSkqHrpk7FM" %}

### Key Features

* Powerful MCU: Microchip ATSAMD51P19 with ARM Cortex-M4F core running at 120MHz
* Reliable Wireless Connectivity: Equipped with Realtek RTL8720DN, dual-band 2.4Ghz / 5Ghz Wi-Fi
* Complete system equipped with Screen + Development Board + Input/Output Interface + Enclosure
* Highly Integrated Design: 2.4” LCD Screen, MCU, IMU, WIFI, BT, and more practical add-ons housed in a compact enclosure with built-in magnets & mounting holes allow you to easily set up your IoT project
* Raspberry Pi 40-pin Compatible GPIO enables installation as a peripheral to the Raspberry Pi
* External onboard multi-functional Grove ports: Compatible with over 300 Plug\&Play Grove modules to explore IoT
* USB OTG Support： can act as a USB host (reads data or signals from a mouse, keyboard, MIDI device, 3D printer, etc.) or USB client (emulates a mouse, keyboard, or MIDI device to a host PC).
* Support Arduino, CircuitPython, Micropython, ArduPy([What is ArduPy?](http://wiki.seeedstudio.com/Wio-Terminal-ArduPy/)), AT Firmware, Visual Studio Code
* Azure Certified Device: Sense and tag real-world data and visualize through Azure IoT Central&#x20;
* TELEC certificated

<figure><img src="../.gitbook/assets/image (3).png" alt=""><figcaption></figcaption></figure>

### Specification[¶](https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/#specification) <a href="#specification" id="specification"></a>

| Main Chip                | Manufacturer Part Number          | ATSAMD51P19      |
| ------------------------ | --------------------------------- | ---------------- |
| Core Processor           | ARM® Cortex®-M4F                  |                  |
| CPU Speed                | 120MHz(Boost up to 200MHz)        |                  |
| Program Memory Size      | 512KB                             |                  |
| External Flash           | 4MB                               |                  |
| RAM Size                 | 192KB                             |                  |
| Operating Temperature    | -40°C \~ 85°C (TA)                |                  |
| LCD Screen               | Resolution                        | 320 x 240        |
| Display Size             | 2.4 inch                          |                  |
| Driver IC                | ILI9341                           |                  |
| Wireless connectivity    | Manufacturer Part Number          | RTL8720DN        |
| KM4 CPU                  | ARM® Cortex®-M4F                  |                  |
| KM0 CPU                  | ARM® Cortex®-M0                   |                  |
| Wi-Fi                    | 802.11 a/b/g/n 1x1, 2.4GHz & 5GHz |                  |
| Bluetooth                | Support BLE5.0                    |                  |
| Hardware Engine          | AES/DES/SHA                       |                  |
| Built-in Modules         | Accelerometer                     | LIS3DHTR         |
| Microphone               | 1.0V-10V -42dB                    |                  |
| Speaker                  | ≥78dB @10cm 4000Hz                |                  |
| Light Sensor             | 400-1050nm                        |                  |
| Infrared Emitter         | 940nm                             |                  |
| Interface                | microSD Card Slot                 | Maximum 16GB     |
| GPIO                     | 40-PIN (Raspberry Pi Compatible)  |                  |
| Grove                    | 2 (Multifunction)                 |                  |
| FPC                      | 20-PIN                            |                  |
| USB Type-C               | Power & USB-OTG                   |                  |
| Operation interface      | 5-Way Switch                      | /                |
| Power/Reset Switch       | /                                 |                  |
| User Defined Button \* 3 | /                                 |                  |
| Enclosure                | Dimension                         | 72mm\*57mm\*12mm |
| Materials                | ABS+PC                            |                  |

### &#x20;Hardware Overview

<figure><img src="../.gitbook/assets/image (23).png" alt=""><figcaption></figcaption></figure>

### Pinout Diagram <a href="#pinout-diagram" id="pinout-diagram"></a>

![](https://files.seeedstudio.com/wiki/Wio-Terminal/img/WioT-Pinout.jpg)

![](https://files.seeedstudio.com/wiki/Wio-Terminal/img/WT-GROVE.jpeg)

{% embed url="https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/" %}

{% embed url="https://www.seeedstudio.com/Wio-Terminal-p-4509.html" %}



## SenseCAP K1100

<figure><img src="../.gitbook/assets/SenseCAP K1100 Kit Poster.jpg" alt=""><figcaption></figcaption></figure>
