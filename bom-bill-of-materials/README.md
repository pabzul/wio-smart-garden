# BOM (Bill Of Materials)

<figure><img src="../.gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>

## Here is the list of materials and software you will need in order to build this project:

### Hardware:

* [WIO Terminal](wio-terminal.md)
* [Grove Moisture Sensor](grove-soil-moisture-sensor.md)
* [Grove Temperature and Humidity Sensor (DHT11)](grove-dht11.md)

### Software:

* [Arduino IDE](../4-install-the-software/arduino-ide.md)
* [WIO terminal and grove sensors libraries](../4-install-the-software/dht-library.md)
