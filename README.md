# Introduction

<figure><img src=".gitbook/assets/Seeed-Logo-in-Colors-long.png" alt=""><figcaption></figcaption></figure>

This workshop was designed by POWAR STEAM, inspired and sponsored by Seeed Studio and their WIO Terminal. An IoT device that has helped empower our POWAR climate simulator and given us a lot of new ideas about its uses in education.

<figure><img src=".gitbook/assets/wio-terminal.png" alt=""><figcaption></figcaption></figure>

A smart garden is a gardening system that uses technology to automate various aspects of plant care. It includes sensors, software, and other devices to monitor soil moisture, light, temperature, and other factors influencing plant growth.

<figure><img src=".gitbook/assets/11 (1).png" alt=""><figcaption></figcaption></figure>

The information collected by these devices is used to control irrigation, fertilization, and other aspects of plant care, with the goal of making gardening more efficient and effective. The aim is to provide ideal growing conditions for plants and optimize their growth while reducing the need for manual labour and guesswork.

<figure><img src=".gitbook/assets/7 (1).jpg" alt=""><figcaption></figcaption></figure>

## Demo of the project:

{% embed url="https://www.youtube.com/watch?v=OoDj6184f5s" %}

## Get the kit to build the project:

{% embed url="https://www.seeedstudio.com/Wio-Terminal-Smart-Garden-Kit-p-4988.html" %}

## The Inspiration:

{% embed url="https://www.youtube.com/watch?v=NQt-XLcSIwA" %}
