# 3 - Connect the sensors

When you are working with most of the microcontroller systems, you would have to look out for the pinouts of the microcontroller and the sensor to understand how you would have to connect it in order to work with your code.&#x20;

The good thing is that when working with the WIO terminal and the grove sensors, these sensors are "plug and play", meaning that you won't have to bother for connections, but instead just plug them, code and use them.

For example, this would be the schematics for connecting our system to an Arduino Uno Microcontroller:



## Digital Soil Moisture Sensor:

<figure><img src=".gitbook/assets/11 - Soils Moisture 1_bb.png" alt=""><figcaption></figcaption></figure>

## Analogue Soil Moisture Sensor:

<figure><img src=".gitbook/assets/11 - Soils Moisture 2_bb.png" alt=""><figcaption></figcaption></figure>

## DHT11:

<figure><img src=".gitbook/assets/10 - DHT11_bb.png" alt=""><figcaption></figcaption></figure>
