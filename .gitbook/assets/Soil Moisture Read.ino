// declare to which Pin is the sensor connected
int sensorPin = A0;

// declare the initial value of the sensor
int sensorValue = 0;
 
void setup() {
    // open the serial port (the communication with the computer to show values)
    Serial.begin(9600);
}

void loop() {
    // read the value from the sensor:
    sensorValue = analogRead(sensorPin);

    // print the value in the serial monitor
    Serial.print("Moisture = " );
    Serial.println(sensorValue);

    // wait one second to start again
    delay(1000);
}