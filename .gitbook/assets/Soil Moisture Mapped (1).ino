// declare to which Pin is the sensor connected
int sensorPin = A0;

// declare the initial value of the sensor
int sensorValue = 0;

void setup() {
    // open the serial port (the communication with the computer to show values)
    Serial.begin(9600);
}

void loop() {
    // read the value from the sensor:
    sensorValue = analogRead(sensorPin);

/* a conditional that tells us that the plant is DRY
if the value is LESS or equal than 300 */
if (sensorValue <= 300) {
    Serial.println("DRY SOIL...PLANT NEEDS WHATER" );
    Serial.print("Moisture = " );
    Serial.println(sensorValue);
}

/* a conditional that tells us that the plant is TOO WET 
if the value is MORE or equal than 700 */
if (sensorValue >= 700) {
    Serial.println("SUPER WET...TOO MUCH WATER" );
    Serial.print("Moisture = " );
    Serial.println(sensorValue);
}

/* a conditional that tells us that the plant is HAPPY 
if the value is LESS than 700 and MORE than 300 */
if (sensorValue < 700 && sensorValue > 300) {
    Serial.println("PERFECT MOISTURE...PLANT IS HAPPY" );
    Serial.print("Moisture = " );
    Serial.println(sensorValue);
}

    // a space between the writing of the readings
    Serial.println();

    // wait one second to start again
    delay(1000);
}