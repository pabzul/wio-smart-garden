# 2 - Assemble the hardware

For building this project you will need:

## [Wio terminal](bom-bill-of-materials/wio-terminal.md)

<figure><img src=".gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

## [Grove Sensor DHT11](bom-bill-of-materials/grove-dht11.md)

<figure><img src=".gitbook/assets/image (19).png" alt=""><figcaption></figcaption></figure>

## [Grove Moisture Sensor](bom-bill-of-materials/grove-soil-moisture-sensor.md)

<figure><img src=".gitbook/assets/image (28).png" alt=""><figcaption></figcaption></figure>

## [Grove Wires: x2](https://www.seeedstudio.com/Grove-Universal-4-Pin-Buckled-20cm-Cable-5-PCs-pack.html?queryID=01fad2f9bda13811b96d46b880b3bd7c\&objectID=1693\&indexName=bazaar\_retailer\_products)

<figure><img src=".gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>
