# Screen Library

## Installing LCD Library:

* Visit the [Seeed\_Arduino\_LCD](https://github.com/Seeed-Studio/Seeed\_Arduino\_LCD) Github repositories and download the entire repo to your local drive.

<figure><img src="../.gitbook/assets/Captura de pantalla_20230213_133311.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Here's the file of the last library V2.2.6 by 13-02-2023 (check g~~i~~t for the latest version)



{% file src="../.gitbook/assets/Seeed_Arduino_LCD-master.zip" %}
{% endhint %}

Open the Arduino IDE, and click `sketch` -> `Include Library` -> `Add .ZIP Library`, and choose the `Seeed_Arduino_LCD` file that you have just downloaded.

<figure><img src="../.gitbook/assets/image (16).png" alt=""><figcaption></figcaption></figure>

### Installing the Adafruit Zero DMA Library: <a href="#installing-the-adafruit-zero-dma-library" id="installing-the-adafruit-zero-dma-library"></a>

Some functions of the TFT LCD Library require this library.

1. Navigate to `sketch` -> `Include Library` -> `Manager Library`, and a library manager window will appear.
2. Search **Adafruit Zero DMA** and click Install.

![](https://files.seeedstudio.com/wiki/Wio-Terminal/img/Xnip2019-12-16\_09-19-28.jpg)

