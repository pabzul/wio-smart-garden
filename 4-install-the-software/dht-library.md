# DHT Library

### Installing DHT Library:

* Visit the [Seeed DHT library](https://github.com/Seeed-Studio/Grove\_Temperature\_And\_Humidity\_Sensor) Github repositories and download the entire repo to your local drive.

{% embed url="https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor" %}
Library Link
{% endembed %}

* Go to the library link, click the green `CODE` button, and select `Download as Zip`

<figure><img src="../.gitbook/assets/image (22).png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Here's the file of the last library V2.01 by 13-02-2023 (check git for the latest version)



{% file src="../.gitbook/assets/Grove_Temperature_And_Humidity_Sensor-master.zip" %}
{% endhint %}

* Open the Arduino IDE, and click `sketch` -> `Include Library` -> `Add .ZIP Library`, and choose the `Grove_Temperature_And_Humidity_Sensor-master` file that you have just downloaded.

<figure><img src="../.gitbook/assets/image (13).png" alt=""><figcaption></figcaption></figure>
