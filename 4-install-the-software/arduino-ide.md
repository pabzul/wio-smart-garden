# Arduino IDE

To install the Arduino IDE click on the link below to open the web page with the installer.&#x20;

{% embed url="https://www.arduino.cc/en/software" %}

* Select the version of Arduino IDE based on the operating system of your computer.

<figure><img src="../.gitbook/assets/image (9).png" alt=""><figcaption></figcaption></figure>

* Decide if you want to donate, or click on "Just Download"

<figure><img src="../.gitbook/assets/image (14).png" alt=""><figcaption></figcaption></figure>

* After downloading the installer, execute it and follow the instructions.
* Open your Arduino IDE, click on **File** -> **Preferences**, and copy the URL below to **Additional Boards Manager URLs**:

```
https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json
```

<figure><img src="../.gitbook/assets/image (8).png" alt=""><figcaption></figcaption></figure>

* Click OK
* Click on **Tools** -> **Board** -> **Board Manager** and Search **Wio Terminal** in the Boards Manager.

<figure><img src="../.gitbook/assets/image (17).png" alt=""><figcaption></figcaption></figure>

## Test the WIO Terminal:

* Connect your WIO Terminal to the computer
* On the Arduino IDE click on **Files ->** **Examples ->** **01. Basics > Blink** (this will open a sample code that will make the WIO Terminal internal LED Blink every second).

<figure><img src="../.gitbook/assets/image (26).png" alt=""><figcaption></figcaption></figure>

* Click on **Tools** -> **Board** -> **Board Manager** and Search **Wio Terminal** in the Boards Manager.

<figure><img src="../.gitbook/assets/image (20).png" alt=""><figcaption></figcaption></figure>

* Select the serial device of the Wio Terminal board from the **Tools -> Port** menu.

<figure><img src="../.gitbook/assets/image (24).png" alt=""><figcaption></figcaption></figure>

* Now, simply click the **Upload** button in the environment. Wait a few seconds and if the upload is successful, the message "Done uploading." will appear in the status bar.

<figure><img src="https://files.seeedstudio.com/wiki/Wio-Terminal/img/upload.png" alt=""><figcaption><p><em>Upload the code</em></p></figcaption></figure>

* Now the Blue led at the bottom of your WIO Terminal should be blinking every second.

