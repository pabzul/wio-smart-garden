# 4 - Install the software

<figure><img src="../.gitbook/assets/image (1).png" alt=""><figcaption></figcaption></figure>

For this specific project, you will need to install the Arduino IDE software in order to code the WIO Terminal, the DHT library to read the DHT11 sensors, and the LCD screen library.

* [Installing Arduino IDE](arduino-ide.md)
* [Installing DHT Library](dht-library.md)
* [Installing LCD Library](screen-library.md)

